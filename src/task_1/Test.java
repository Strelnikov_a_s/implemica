package task_1;

public class Test {
    public static void main(String[] args) {
        if(Solution_1.getBktCombinationsCount(1)!=1 |
                 Solution_1.getBktCombinationsCount(2) != 2 |
                    Solution_1.getBktCombinationsCount(4) != 14 |
                        Solution_1.getBktCombinationsCount(6) != 132 ) {
            throw new AssertionError();
        }else System.out.println("Test for task 1 complete!");
    }
}
