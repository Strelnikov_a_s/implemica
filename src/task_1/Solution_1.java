package task_1;


public class Solution_1 {
    //Number of detected combinations
    private static int currentCombinationsCount;
    /**
    *static util method for external access
    * @param fullBktPairNumber - number of given pair brackets
    */
    public static int getBktCombinationsCount(int fullBktPairNumber) {
        currentCombinationsCount = 0;
        bktCombinationsCounter(0, 0, fullBktPairNumber);
        return currentCombinationsCount;
    }

    /**
     * recursive method simulates building of correct brackets sequence
     * we add left bracket while it's number < brackets pair
     * then we add right bracket while left & right brackets < pair
     * at first step we has sequence like ((()))
     * second step like (()())
     * third (())()
     * @param leftBktCount - number of added left brackets
     * @param rightBktCount
     * @param fullBktPairNumber - number of given pair brackets.never changed
     */
    private static void bktCombinationsCounter(int leftBktCount, int rightBktCount, int fullBktPairNumber){
        if(fullBktPairNumber < 1) {
            throw new IllegalArgumentException("fullBktPairNumber can't be negative o zero");

        }
        //++currentCombinationsCount when we have correct sequence
        if(leftBktCount + rightBktCount == fullBktPairNumber * 2){
            currentCombinationsCount = currentCombinationsCount + 1;
            return;
        }
        // add left brackets
        if(leftBktCount < fullBktPairNumber){
            bktCombinationsCounter(leftBktCount + 1, rightBktCount, fullBktPairNumber);
        }

        //we add right bracket when left bracket count == max available
        // or when we recursively return and delete last added left bracket

        if(rightBktCount < leftBktCount){
            bktCombinationsCounter(leftBktCount, rightBktCount + 1, fullBktPairNumber);
        }


    }
}
