package task_2;


import java.util.Scanner;

/**
 *
 * this class read input data from console
 * create Dijkstra object and start algorithm
 * it worked for many number of test
 */
public class Solution_2 {
    //Console scanner
    private Scanner scn = new Scanner(System.in);

    public void startTest() {
        System.out.println("Enter data");
        //first we read a number of test
        int numberOfTests = scn.nextInt();
        //skip to next line
        scn.nextLine();

        for (int i = 0; i < numberOfTests; i++){
            testSolution();
            scn.nextLine();
        }
    }
    private void testSolution(){
        //read number of city
        int numberOfCities = scn.nextInt();
        scn.nextLine();
        //create Dijkstra
        Dijkstra dijkstra = new Dijkstra(numberOfCities);
        //cities cycle
        for(int i = 0; i< numberOfCities; i++) {

            String cityName = scn.nextLine().trim();
            //read number of neighbors
            int numberOfNeighbours = scn.nextInt();
            scn.nextLine();
            //create array for all cities.where index - city id
            int[] neighbours = new int[numberOfCities+1];
            for (int j = 0; j < numberOfNeighbours; j++) {
                //read city id
                int nrId = scn.nextInt();
                // read distance from current city to this id
                int distance = scn.nextInt();
                //skip to next line
                scn.nextLine();
                //add distance to array
                neighbours[nrId] = distance;
            }

            dijkstra.addNewCity(cityName,neighbours);
        }
        //read number of path
        int numberOfPathToFind = scn.nextInt();
        scn.nextLine();
        //array for results
        int [] output = new int[numberOfPathToFind];
        for (int i = 0; i< numberOfPathToFind; i++){

            String sourceCity = scn.next().trim();
            String destinationCity = scn.nextLine().trim();
            dijkstra.setStartPoint(sourceCity);
            dijkstra.setEndPoint(destinationCity);
            //can't get a result while start and end points not defined
            int result = dijkstra.gitMinDistance();

            output[i] = result;
        }
        //print results
        for (int i = 0;i<output.length;i++){
            System.out.println(output[i]);
        }


    }
}
