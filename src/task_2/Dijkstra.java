package task_2;

/**
 * class for find min path between two graph point by Dijkstra algorithm
 */
import java.util.HashMap;

class Dijkstra {
    // Map<City name , City id>
    private HashMap<String,Integer> cities;
    // id of last added city
    private int lastId;
    //source city id
    private int startPoint;
    // destination city id
    private int endPoint;
    private static final int INFINITY = Integer.MAX_VALUE/2;
    // storage distances from start point to current point
    // where distances[0] always positive infinity because id of first city - 1
    // index of element in this array - city id
    private int distances[];
    // graph of cities. line and column index - city id
    // graph[firstCityId][secondCityId] - distance between this cities
    private int [][] graph;
    // array of visited point
    private boolean [] visited;
    // size of visited, graph and distances
    private int arraysSize;


    protected Dijkstra(int citiesNumber) {
        cities = new HashMap<>(citiesNumber);
        //because array has zero index and city hasn't zero id
        arraysSize = citiesNumber+1;
        graph = new int[arraysSize][arraysSize];

        initVisited(arraysSize);
    }
    // create array of distance and set INFINITY  each cell
    private void initDistances(int arraysSize){
        distances = new int[arraysSize];
        for (int i = 0; i < distances.length; i++){
            distances[i] = INFINITY;
        }

    }

    private void initVisited(int arraysSize){
        visited = new boolean[arraysSize];
        //because it's not working cell
        visited[0] = true;
    }
    //find city id with min distance
    private int getNextId(){
        int minId = 0;
        int min = distances[0];
        for (int i = 0; i< distances.length; i++){
            if(distances[i]<min && !visited[i]){
                min = distances[i];
                minId = i;
            }
        }
        return minId;
    }
    //set INFINITY each cell
    private void normalizeGraph(int [][] myGraph){
        for (int i = 0;i < myGraph.length;i++){
            for (int j = 0;j < myGraph[i].length; j++){
                if(myGraph[i][j]==0) myGraph[i][j] = INFINITY;
            }
        }
    }
    //add City name and id to cities
    //set distance between cities in graph
    //@param neighbors - array of distances from this city to each other
    protected void addNewCity(String name,int[] neighbours){
        cities.put(name,++lastId);
        graph[lastId] = neighbours;
    }
    // when we find min cost to end point distances[endPoint] is result.
    private boolean hasResult(){
        if(visited[endPoint])return true;
        return false;
    }
    // main method of this class
    // it's search a result
    public int gitMinDistance(){
        initDistances(arraysSize);
        normalizeGraph(graph);
        //our result
        int solution;
        // distance between start and current point
        int distance = 0;
        while (!hasResult()) {
            //array of distances from current point to another cities
            // where index of array is city id
            int [] m = graph[startPoint];
            for (int i = 0; i < m.length; i++) {
                //if we hasn't min distance to this city
                // and if (distance from start point to current point) + distance from current point to city (m[i]) < detected distance
                if (!visited[i] && (m[i]+distance) < distances[i]){
                    //we update detected distance
                    distances[i] = m[i]+distance;
                }
            }

            startPoint = getNextId();
            distance = distances[startPoint];
            visited[startPoint] = true;
        }
        //when we has result
        solution = distances[endPoint];
        //update distances and visited to next test
        reInitDijkstra();
        return solution;
    }
    //set startPoint(get id by name)
    protected void setStartPoint(String startCityName) {
        this.startPoint = cities.get(startCityName);
        visited[startPoint] = true;
    }
    //like start point
    protected void setEndPoint(String endCityName) {
        this.endPoint = cities.get(endCityName);
    }
    //update distances and visited to next test
    private void reInitDijkstra(){
        initDistances(arraysSize);
        initVisited(arraysSize);
    }
}
