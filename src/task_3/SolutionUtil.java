package task_3;


import java.math.BigInteger;


public class SolutionUtil {
    // return sum of digits in string.
    public static int getSumOfDigits(String s){
        int result = 0;
        for (char c:s.toCharArray()){
            result += Character.getNumericValue(c);
        }
        return result;
    }

    public static BigInteger getFactorial(int n){
        // 1! = 0! = 1
        if (n == 1 || n == 0 ) return new BigInteger("1");
        //n! = n * (n-1)!
        return new BigInteger(String.valueOf(n)).multiply(getFactorial(n-1));
    }
}
