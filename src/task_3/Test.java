package task_3;


import java.math.BigInteger;

public class Test {
    public static void main(String[]args){
        BigInteger factorial = SolutionUtil.getFactorial(100);
        System.out.println(SolutionUtil.getSumOfDigits(factorial.toString()));
    }
}
